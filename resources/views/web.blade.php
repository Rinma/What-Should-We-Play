<!DOCTYPE html>
<html lang="en">
    <head>
        <title>WSWP</title>
        <meta charset="UTF-8">
    </head>
    <body>
        <div id="app">
            <root></root>
        </div>
        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
