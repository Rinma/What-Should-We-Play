require('./bootstrap');

// Init Vue and components
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuetify from "vuetify";

Vue.use(VueRouter);
Vue.use(Vuetify);

// The app root component
import Root from './Root';

// Vue Routes / Pages
import Home from './pages/Home';

// Config vue components
const vuetify = new Vuetify({});

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        }
    ]
});

// Start vue app
new Vue({
    el: '#app',
    components: {
        Root
    },
    router,
    vuetify
});
